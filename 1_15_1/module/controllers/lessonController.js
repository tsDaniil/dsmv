/**
 * Created by daniil on 13.08.14.
 */
function lessonController($scope, $location, $uiModal) {

    if (!Array.prototype.shuffle) {
        Array.prototype.shuffle = function () {
            for (var i = this.length - 1; i > 0; i--) {
                var num = Math.floor(Math.random() * (i + 1));
                var d = this[num];
                this[num] = this[i];
                this[i] = d;
            }
            return this;
        };
    }

    var ScopeController = {

        run: function () {
            this.initLesson();
            this.setLessonData();
        },
        setHandlers: function () {

            var Controller = this;

            var count = 0;

            function isAllSuccess() {
                return count == 4;
            }

            setTimeout(function () {
                $(".btns label").bind("click", function () {

                    var $this = $(this);

                    var sign = $this.attr("data-sign"),
                        userChoise = $this.text();

                    if (sign == userChoise) {
                        $this.removeClass("btn-default").removeClass("btn-danger").addClass("btn-success active");
                        $this.unbind();
                        if ($this.next()) {
                            $this.next().unbind();
                        }
                        if ($this.prev()) {
                            $this.prev().unbind();
                        }
                        count++;
                        if (isAllSuccess()) {
                            Controller.setSuccess();
                        }
                    } else {
                        $this.removeClass("btn-default").addClass("btn-danger");
                    }

                });

            }, 1000);

        },

        setSuccess: function () {

            var parentScope = $scope;

            var modalInstance = $uiModal.open({
                templateUrl: activeModule + '/tpl/lessonComplete.tpl.html',
                controller: function ($scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.parentScope = parentScope;

                }
            });

            modalInstance.result.then(function () {
                location.reload();
            }, function () {
                location.reload();
            });
        },

        setLessonData: function () {

            var lessonData = [
                {type: "flayer", content: [
                    [3, 1],
                    [3, 2]
                ], sign: "<"},
                {type: "fish", content: [
                    [3, 2],
                    [2, 2]
                ], sign: ">"},
                {type: "beard", content: [
                    [2, 2],
                    [2, 1]
                ], sign: ">"},
                {type: "leaf", content: [
                    [2, 1],
                    [2, 2]
                ], sign: "<"}
            ];

            lessonData = lessonData.map(function (data) {

                data.content = data.content.map(function (arr) {

                    var resutl = [];

                    for (var i = 0; i < arr[0] + arr[1]; i++) {
                        resutl.push((i < arr[0] ? 1 : 2));
                    }

                    resutl = resutl.shuffle();

                    return resutl;
                });

                return data;
            });

            $scope.lessonData = lessonData;
        },
        initLesson: function () {
            if (localStorage.getItem(this.getCompleteStr())) {
                $scope.complete = true;
                this.setSigns(JSON.parse(localStorage.getItem(this.getDataStr())));
            } else {
                this.setHandlers();
            }
        },
        getCompleteStr: function () {
            return this.getPrefix() + ":complete";
        },
        getDataStr: function () {
            return this.getPrefix() + ":data";
        },
        getPrefix: function () {
            return activeModule + "/";
        },
        setSigns: function (data) {
            $scope.signs = data;
        },
        saveState: function () {

        }
    };

    ScopeController.run();
}
