/**
 * Created by daniil on 13.08.14.
 */
angular.module(activeModule, [])
    .config(function ($stateProvider) {

        $stateProvider
            .state('lesson', {
                url: "/" + activeModule + "/lesson",
                views: {
                    main: {
                        templateUrl: activeModule + "/tpl/general.tpl.html"
                    }
                }
            });
    });

Main.addScript(activeModule + "/module/controllers/lessonController.js");
Main.addStyle(activeModule + "/css/style.css");