/**
 * @class Drag
 * @extends Base
 */
var Drag = Base.extend({
    /**
     * @constructor
     * @param {$} img
     * @param {Start} parent
     * @param {Boolean} [complete]
     */
    constructor: function Drag(img, parent, complete) {

        /**
         * @type {Start}
         */
        this.parent = parent;
        /**
         * @type {$}
         */
        this.img = img;

        this.complite = complete || false;

        this.init();
    },

    /**
     * Инициализация
     */
    init: function () {

        this.container = $(".container");

        this.saveCoords();

        this.setHandlers();

    },

    /**
     * Сохраняем начальные координаты
     * @returns {Drag}
     */
    saveCoords: function () {
        this.img.attr("data-coords", JSON.stringify({
            x: parseInt(this.img.css("left")),
            y: parseInt(this.img.css("top"))
        }));
        return this;
    },

    /**
     * Получаем начальные координаты
     * @returns {Object}
     */
    getStartCoords: function () {
        return JSON.parse(this.img.attr("data-coords"));
    },

    /**
     * Статус (находится ли элемент в нужной строке)
     * @returns {boolean}
     */
    isComplite: function () {
        return this.complite;
    },

    /**
     * Метод возвращает картинку на исходную точку
     */
    returnToStart: function () {
        var coords = this.getStartCoords();
        this.img.animate({
            "left": coords.x + "px",
            "top": coords.y + "px"
        });
    },

    /**
     * Меняем статус
     * Снимаем обработчики
     */
    setComplete: function () {
        this.complite = true;
        this.img.unbind();
    },

    /**
     * Назначаем обработчики
     */
    setHandlers: function () {

        this.img.bind({
            "mousedown": function (e) {

                this.mouseDown(this.getCoordsByEvent(e));

                $(document).bind({
                    "mousemove": function (e) {

                        this.mouseMove(this.getCoordsByEvent(e));

                    }.bind(this),

                    "mouseup": function (e) {

                        this.mouseUp(this.getCoordsByEvent(e));

                        $(document).unbind("mousemove");
                        $(document).unbind("mouseup");

                    }.bind(this)
                });

                return false;
            }.bind(this)
        });

    },

    /**
     * Сбрасываем классы подсветки контейнеров
     */
    dropClasses: function () {
        this.container.removeClass("hover");
    },

    /**
     * Обрабатываем событие нажатия на картинку
     * @param {Object} coords
     * @param {Number} coords.x
     * @param {Number} coords.y
     */
    mouseDown: function (coords) {

        this.start = coords;
        this.delta = this.currentDelta(coords);

        this.img.css("z-index", 2);

    },

    /**
     * Передвигаем картинку
     * Подсвечиваем контейнер при наведении
     * @param {Object} coords
     * @param {Number} coords.x
     * @param {Number} coords.y
     */
    mouseMove: function (coords) {

        var container = this.parent.getContainer(coords);

        this.dropClasses();

        if (container) {
            container.addClass("hover");
        }

        this.img.css({
            "left": coords.x + this.delta.x + "px",
            "top": coords.y + this.delta.y + "px"
        });

    },

    /**
     * Отпускаем картинку
     * Генерируем событие на родителе что картинка отпущена
     * @param {Object} coords
     * @param {Number} coords.x
     * @param {Number} coords.y
     */
    mouseUp: function (coords) {

        this.img.css("z-index", 1);

        this.dropClasses();

        var elemCoords = {
            x: coords.x + this.delta.x,
            y: coords.y + this.delta.y
        };

        this.parent.trigger("dragEnd", [this.img, this, coords, elemCoords]);

    },

    /**
     * Рассчитываем расстояния от мышки до левого и верхнего края картинки
     * (Чтобы картинка не дергалась при таскании)
     * @param {Object} coords
     * @param {Number} coords.x
     * @param {Number} coords.y
     * @returns {{x: number, y: number}}
     */
    currentDelta: function (coords) {
        var offset = this.img.offset();
        var contentOffset = $(".content").offset();
        return {
            x: offset.left - contentOffset.left - coords.x,
            y: offset.top - contentOffset.top - coords.y
        };
    },

    /**
     * Получаем координаты мыши по событию
     * @param {Event} event
     * @returns {{x: (Number|pageX|*), y: (Number|pageY|*)}}
     */
    getCoordsByEvent: function (event) {
        return {
            x: event.pageX, y: event.pageY
        }
    }
});
