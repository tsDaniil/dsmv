/**
 * Created by daniil on 12.08.14.
 */

/**
 * @class Main
 * @extends Base
 */
var Main = new (Base.extend({

    /**
     * @constructor
     */
    constructor: function Main() {

        this.initActiveLesson();
        this.setNewLocalStorage();

    },

    /**
     * Добавляем скрипт
     * @param {String} path
     * @param {function} [callback]
     * @return {HTMLElement}
     */
    addScript: function (path, callback) {

        if (document.readyState == "complete") {

            var script = document.createElement("script");
            script.onload = callback;
            script.onerror = this.errorLoad;
            script.onabort = this.errorLoad;
            script.src = path;
            document.head.appendChild(script);
            return script;

        } else {
            document.write("<script src='" + path + "'></script>");
        }

    },

    /**
     * Делаем путь ло модуля
     * @return {string}
     */
    getMainPath: function () {
        return activeModule + "/module/" + activeModule + ".js";
    },

    /**
     * Добавляем стиль
     * @param {String} path
     * @return {HTMLElement}
     */
    addStyle: function (path) {

        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.onabort = this.errorLoad;
        link.onerror = this.errorLoad;
        link.href = path;
        document.head.appendChild(link);
        return link;
    },

    /**
     * Обработка ошибки загрузки
     */
    errorLoad: function () {
        console.error("Load error!");
        console.error(arguments);
    },

    /**
     * Инициализируем активный урок
     */
    initActiveLesson: function () {

        var search = this.getSearch();

        if ("lesson" in search) {
            activeModule = search["lesson"];
        } else {
            activeModule = "default";
        }

        if (activeModule == "default") {

            location.hash = "#/default";

        }

    },

    /**
     * Получаем активный урок
     * @return {{}}
     */
    urlParse: function () {

        var search = {};
        var str = window.location.hash;

        if (str == "") {
            return search;
        }

        str = str.replace("#/", "");
        str = str.split("/");

        if (str.length) {
            search.lesson = str[0];
        }

        if (str[2]) {
            search.task = str[2];
        }

        this.search = search;
        return search;
    },

    /**
     * Получаем распарсенный
     * @return {Object}
     */
    getSearch: function () {
        return this.search || this.urlParse();
    },

    isDebug: function () {
        return location.href.indexOf("debug") != -1;
    },

    setNewLocalStorage: function () {

        if (this.isDebug()) {
            return this;
        }

        /**
         * @type {Object}
         */
        window.localStorage = new Object();

        window.localStorage.getItem = function (name) {
            return prompt("get " + name);
        };

        window.localStorage.setItem = function (name, value) {
            prompt("set " + name + " " + value);
        }
    }

}));
