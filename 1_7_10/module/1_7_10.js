/**
 * Created by daniil on 13.08.14.
 */
angular.module("1_7_10", [])
    .config(function ($stateProvider) {

        $stateProvider
            .state('lesson', {
                url: "/1_7_10/lesson/{lessonNumber}",
                views: {
                    main: {
                        templateUrl: "1_7_10/tpl/general.tpl.html"
                    }
                }
            });
    });

Main.addScript("1_7_10/module/controllers/lessonController.js");
Main.addStyle("1_7_10/css/style.css");