/**
 * Created by daniil on 13.08.14.
 */
function lessonController($scope, $state, $location, $uiModal, $log) {

    var ScopeController = {
        run: function () {
            this.setLessonNumber(this.getLessonNumber());
            this.initLesson();
            this.setLessonNumberToMain();
            this.setTaskText();
            this.setHandlers();
        },
        setLessonNumberToMain: function () {
            Main.lesson = $scope.taskNumber;
        },
        getLessonNumber: function () {
            return parseInt($state.params.lessonNumber);
        },
        setLessonNumber: function (lessonNumber) {
            $scope.taskNumber = this.lessonNumber = lessonNumber;
            return this;
        },
        setTaskText: function () {
            $scope.task = this.getTaskTextByNum(this.lessonNumber);
        },
        getTaskTextByNum: function (number) {
            var tasks = [
                {
                    text: "Собери в одну строчку чётные числа, а в другую — нечётные.",
                    first: "Четные",
                    second: "Нечетные",
                    firstClass: "container even",
                    secondClass: "container odd"
                },
                {
                    text: "Собери в одну строчку однозначные числа, а в другую — двузначные числа до 20.",
                    first: "Однозначные",
                    second: "Двузначные меньше 20",
                    firstClass: "container one-figure",
                    secondClass: "container"
                },
                {
                    text: "Собери в одну строчку двузначные числа до 20, а в другую — круглые числа.",
                    first: "Двузначные меньше 20",
                    second: "Круглые числа",
                    firstClass: "container",
                    secondClass: "container round"
                }
            ];
            return tasks[number - 1];
        },
        setHandlers: function () {

            var parentScope = $scope;

            $scope.prev = function () {
                $location.path($location.path().replace("lesson/" + $scope.taskNumber, "lesson/" + ($scope.taskNumber - 1)));
                location.hash = $location.path();
                location.reload();
            };

            $scope.next = function () {
                $location.path($location.path().replace("lesson/" + $scope.taskNumber, "lesson/" + ($scope.taskNumber + 1)));
                location.hash = $location.path();
                location.reload();
            };

            Main.bind("lesson complete", function () {

                this.saveState();

                var modalInstance = $uiModal.open({
                    templateUrl: '1_7_10/tpl/lessonComplete.tpl.html',
                    controller: function ($scope, $modalInstance) {

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.parentScope = parentScope;

                    }
                });

                modalInstance.result.then(function () {
                    location.reload();
                }, function () {
                    location.reload();
                });


            }.bind(this));
        },
        initLesson: function () {
            if (localStorage.getItem(this.getCompleteStr())) {
                $scope.complete = true;
                this.setCoordsForBlocks(JSON.parse(localStorage.getItem(this.getDataStr())));
            } else {
                Main.addScript("js/general/Drag.js", function () {
                    Main.addScript("1_7_10/js/start.js");
                });
            }
        },
        getCompleteStr: function () {
            return this.getPrefix() + ":complete";
        },
        getDataStr: function () {
            return this.getPrefix() + ":data";
        },
        getPrefix: function () {
            return activeModule + "/" + this.lessonNumber;
        },
        getCoordsOfBlocks: function () {

            var coords = [], figures = $(".img");

            figures.each(function () {
                coords.push({left: $(this).css("left"), top: $(this).css("top")});
            });

            return coords;
        },
        setCoordsForBlocks: function (coords) {

            var figures = $(".img");
            coords.forEach(function (coords, index) {
                figures.eq(index).css(coords);
            });

        },
        saveState: function () {
            var coords = this.getCoordsOfBlocks();
            localStorage.setItem(this.getDataStr(), JSON.stringify(coords, "", 4));
            localStorage.setItem(this.getCompleteStr(), "true");
        }
    };

    ScopeController.run();
}
