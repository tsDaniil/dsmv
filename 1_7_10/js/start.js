/**
 * @class Start
 * @extends Base
 */
var Start = Base.extend({

    /**
     * @constructor
     */
    constructor: function Start() {

        this.init();

    },

    /**
     * Инициализация
     */
    init: function () {

        this.containers = $(".container");
        this.lessonNumber = this.getLessonNumber();

        this.createImg();
        this.setHandlers();

    },

    /**
     * Получаем номер активного урока
     * @return {Number}
     */
    getLessonNumber: function () {
        return Main.lesson;
    },

    /**
     * Инициализируем перетаскивание картинок по разным правилам в зависимости от урока
     */
    createImg: function () {

        this.drag = [];
        if (this.lessonNumber == "1") {

            $(".img").each(function (index, img) {
                this.drag.push(new Drag($(img), this));
            }.bind(this));

        } else if (this.lessonNumber == "2") {

            $(".img").each(function (index, img) {

                var num = this.getNumber($(img));

                if (!this.isOneFigure(num) && num >= 20) {
                    this.drag.push(new Drag($(img), this, true));
                } else {
                    this.drag.push(new Drag($(img), this));
                }

            }.bind(this));

        } else {

            $(".img").each(function (index, img) {

                var num = this.getNumber($(img));

                if (this.isOneFigure(num)) {
                    this.drag.push(new Drag($(img), this, true));
                } else {
                    this.drag.push(new Drag($(img), this));
                }


            }.bind(this));

        }

    },

    /**
     * Назначаем обработчики
     */
    setHandlers: function () {

        if (this.lessonNumber == "1") {

            this.setHandlersFirstLesson();

        } else if (this.lessonNumber == "2") {

            this.setHandlersSecondLesson()

        } else {

            this.setHandlersTherd();

        }


        this.bind("dragEnd", function () {

            if (this.isAllComplete()) {
                Main.trigger("lesson complete", this.lessonNumber);
            }

        }.bind(this));

    },

    /**
     * Назначаем обработчики для первого урока
     * Проверяет четное не четное
     */
    setHandlersFirstLesson: function () {

        this.bind("dragEnd", function (img, Drag, mouseCoord) {

            var container = this.getContainer(mouseCoord);

            if (container) {
                if (container.hasClass("even")) {
                    if (!this.isOdd(this.getNumber(img))) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                } else {
                    if (this.isOdd(this.getNumber(img))) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                }
            } else {
                Drag.returnToStart();
            }

        }.bind(this));

    },

    /**
     * Назначаем обработчики для второго урока
     */
    setHandlersSecondLesson: function () {

        this.bind("dragEnd", function (img, Drag, mouseCoord) {

            var container = this.getContainer(mouseCoord);

            if (container) {

                if (container.hasClass("one-figure")) {
                    if (this.isOneFigure(this.getNumber(img))) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                } else {
                    if (!this.isOneFigure(this.getNumber(img)) && this.getNumber(img) < 20) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                }

            } else {
                Drag.returnToStart();
            }

        }.bind(this));

    },

    /**
     * Назначаем обработчики 3-го урока
     */
    setHandlersTherd: function () {

        this.bind("dragEnd", function (img, Drag, mouseCoord) {

            var container = this.getContainer(mouseCoord);
            var num = this.getNumber(img);

            if (container) {

                if (container.hasClass("round")) {
                    if (!this.isOneFigure(num) && this.isNumberRound(num)) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                } else {
                    if (!this.isOneFigure(num) && num < 20) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                }

            } else {
                Drag.returnToStart();
            }

        }.bind(this));

    },

    /**
     * Проверяем круглое ли число (двузначное)
     * @param {Number} num
     * @return {boolean}
     */
    isNumberRound: function (num) {
        return (num + "")[1] == "0";
    },

    /**
     * Проверяем все ли фрукты перенесены куда надо
     * @return {boolean}
     */
    isAllComplete: function () {

        return !(this.drag.some(function (Drag) {
            return !(Drag.isComplite());
        }));

    },

    /**
     * Получаем класс зоны для перетаскивания
     * @param {String} className
     * @return {string}
     */
    getClassName: function (className) {
        var arrNames = className.split(" ");
        arrNames = arrNames.filter(function (elem) {
            if (elem != "error" && elem != "success") {
                return true;
            }
        });

        return arrNames.join(" ");
    },

    /**
     * Расставляем фрукты на полке
     * @param img
     * @param container
     */
    fix: function (img, container) {

        this.success(container);

        var offset = container.offset();
        var contentOffset = $(".content").offset();

        var className = this.getClassName(container.get(0).className);

        if (!this.complete) {
            this.complete = {};
        }

        if (!this.complete[className]) {
            this.complete[className] = [];
        }

        this.complete[className].push(img);

        img.animate({
            left: offset.left - contentOffset.left + ((this.complete[className].length - 1) * 100) + 10,
            top: offset.top - contentOffset.top + (container.height() - img.height()) / 2
        })

    },

    /**
     * Включаем мерцание
     * @param container кто мерцает
     * @param className чем мерцает
     */
    showFlicker: function (container, className) {
        var remove = function () {
                container.removeClass(className);
            },
            add = function () {
                container.addClass(className);
            };

        add();

        setTimeout(function () {
            remove();
        }, 400);
    },

    /**
     * Включаем мерцание красным
     * @param container
     */
    error: function (container) {
        this.showFlicker(container, "error");
    },

    /**
     * Включаем мерцание зеленым
     * @param container
     */
    success: function (container) {
        this.showFlicker(container, "success");
    },

    /**
     * Получаем число в фрукте
     * @param img
     * @return {Number}
     */
    getNumber: function (img) {
        return parseInt(img.attr("data-number"));
    },

    /**
     * Проверяем нечетное ли число
     * @param number
     * @return {number}
     */
    isOdd: function (number) {
        return number % 2;
    },

    /**
     * проверяем однозначное ли число
     * @param number
     * @return {boolean}
     */
    isOneFigure: function (number) {
        return !((number + "").length - 1);
    },

    /**
     * Проверяем попали ли мы в контейнер и если попали то в какой
     * @param {Object} coords
     * @param {Number} coords.x
     * @param {Number} coords.y
     * @return {undefined|$}
     */
    getContainer: function (coords) {

        var result = undefined;

        this.containers.each(function (index, container) {

            if (this.isOnContainer($(container), coords)) {
                result = $(container);
            }

        }.bind(this));

        return result;
    },

    /**
     * Проверяем попали ли мы в контейнер
     * @param container
     * @param coords
     * @return {boolean}
     */
    isOnContainer: function (container, coords) {
        var offset = container.offset();
        return coords.x >= offset.left && coords.x <= offset.left + container.width() &&
            coords.y >= offset.top && coords.y <= offset.top + container.height();
    }

});

var start = new Start();