/**
 * Created by daniil on 13.08.14.
 */
angular.module("1_1_7", [])
    .config(function ($stateProvider) {

        $stateProvider
            .state('lesson', {
                url: "/1_1_7/lesson/{lessonNumber}",
                views: {
                    main: {
                        templateUrl: "1_1_7/tpl/general.tpl.html"
                    }
                }
            });
    });

Main.addScript("1_1_7/module/controllers/lessonController.js");
Main.addStyle("1_1_7/css/style.css");