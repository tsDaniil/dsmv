/**
 * Created by Daniil on 16.08.2014.
 */
/**
 * @class FigureCreater
 * @extends Base
 */
var FigureCreater = Base.extend({
    /**
     * @param figures
     * @constructor
     */
    constructor: function FigureCreater(figures) {
        this.figures = figures;
        this.init();
    },
    /**
     * Инициализация
     */
    init: function () {
        this.addCanvas();
        this.drawFigure();
    },
    /**
     * Добавляем холст для рисования фигуры в каждый элемент набора
     */
    addCanvas: function () {
        var that = this;
        this.figures.each(function () {
            $(this).append(that.getCanvasHtml($(this).width()));
        });
    },
    /**
     * Получаем шаблон холста нужного размера
     * @param {Number} size
     * @returns {string}
     */
    getCanvasHtml: function (size) {
        return "<canvas width='" + size + "' height='" + size + "'></canvas>";
    },
    /**
     * Начинаем отрисовку фигур
     */
    drawFigure: function () {

        var that = this;
        this.figures.each(function () {

            var $this = $(this),
                ctx = that.getContext($this),
                size = {
                    width: $this.width(),
                    height: $this.height()
                };

            if (FigureCreater.isRect($this)) {
                that.drawRect(ctx, size);
            } else if (FigureCreater.isRound($this)) {
                that.drawRound(ctx, size);
            } else {
                that.drawTriangle(ctx, size);
            }

        });
    },
    /**
     * Рисуем квадраты
     * @param {CanvasRenderingContext2D} ctx
     * @param size
     */
    drawRect: function (ctx, size) {
        ctx.rect(0, 0, size.width, size.height);
        ctx.fillRect(1, 1, size.width - 2, size.height - 2);
        ctx.stroke();
    },
    /**
     * Рисуем круги
     * @param {CanvasRenderingContext2D} ctx
     * @param size
     */
    drawRound: function (ctx, size) {
        ctx.arc(size.width / 2, size.height / 2, size.width / 2 - 1, 0, Math.PI * 2, false);
        ctx.fill();
        ctx.stroke();
    },
    /**
     * Рисуем треугольники
     * @param {CanvasRenderingContext2D} ctx
     * @param size
     */
    drawTriangle: function (ctx, size) {
        ctx.moveTo(0, size.height);
        ctx.lineTo(size.width, size.height);
        ctx.lineTo(size.width / 2, 0);
        ctx.lineTo(0, size.height);
        ctx.stroke();
        ctx.fill();
    },
    /**
     * Получаем контекст канваса, устанавливаем исходные параметры
     * @param figure
     * @returns {CanvasRenderingContext2D}
     */
    getContext: function (figure) {
        var ctx = figure.children().get(0).getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = this.getColor(figure);
        ctx.lineWidth = 1;
        return ctx;
    },
    /**
     * Получаем цвет фигуры
     * @param figure
     * @returns {string}
     */
    getColor: function (figure) {
        if (figure.attr("data-color") === "red") {
            return "#f15940";
        } else if (figure.attr("data-color") === "blue") {
            return "#00a1e0";
        } else {
            return "#a5cf4e";
        }
    },
    static: {
        /**
         * Проверяем квадрат ли это
         * @param figure
         * @returns {Boolean}
         */
        isRect: function (figure) {
            return figure.attr("data-figure") === "rect";
        },
        /**
         * Проверяем круг ли это
         * @param figure
         * @returns {Boolean}
         */
        isRound: function (figure) {
            return figure.attr("data-figure") === "round";
        }
    }
});