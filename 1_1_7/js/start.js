/**
 * @class Start
 * @extends Base
 */
var Start = Base.extend({

    constructor: function Start() {

        this.blickClass = "figure";
        this.figures = {};
        this.init();

    },

    init: function () {

        new FigureCreater($("." + this.blickClass));
        this.containers = $(".block");
        this.lessonNumber = this.getLessonNumber();

        this.createImg();
        this.setHandlers();

    },

    getLessonNumber: function () {
        return Main.lesson;
    },

    createImg: function () {

        this.drag = [];

        $("." + this.blickClass).each(function (index, img) {
            this.drag.push(new Drag($(img), this));
        }.bind(this));

    },

    setHandlers: function () {

        if (this.lessonNumber == "1") {

            this.setHandlersFirstLesson();

        } else if (this.lessonNumber == "2") {

            this.setHandlersSecondLesson()

        } else {

            this.setHandlersTherd();

        }


        this.bind("dragEnd", function () {

            if (this.isAllComplete()) {
                Main.trigger("lesson complete", [this.lessonNumber]);
            }

        }.bind(this));

    },

    hasContainresWith: function (figuere) {
        return (figuere in this.figures);
    },

    saveFigure: function (figure, container, type) {
        container.addClass(figure);
        container.attr(type, figure);
        this.figures[figure] = container;
    },

    setHandlersFirstLesson: function () {

        this.bind("dragEnd", function (img, Drag, mouseCoord) {

            var container = this.getContainer(mouseCoord);

            if (container) {
                if (container.attr("data-figure")) {
                    if (img.attr("data-figure") === container.attr("data-figure")) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                } else {
                    if (this.hasContainresWith(img.attr("data-figure"))) {
                        Drag.returnToStart();
                        this.error(container);
                    } else {
                        this.fix(img, container);
                        Drag.setComplete();
                        this.saveFigure(img.attr("data-figure"), container, "data-figure");
                    }
                }
            } else {
                Drag.returnToStart();
            }

        }.bind(this));

    },

    setHandlersSecondLesson: function () {

        this.bind("dragEnd", function (img, Drag, mouseCoord) {

            var container = this.getContainer(mouseCoord);

            if (container) {
                if (container.attr("data-size")) {
                    if (img.attr("data-size") === container.attr("data-size")) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                } else {
                    if (this.hasContainresWith(img.attr("data-size"))) {
                        Drag.returnToStart();
                        this.error(container);
                    } else {
                        this.fix(img, container);
                        Drag.setComplete();
                        this.saveFigure(img.attr("data-size"), container, "data-size");
                    }
                }
            } else {
                Drag.returnToStart();
            }

        }.bind(this));

    },

    setHandlersTherd: function () {

        this.bind("dragEnd", function (img, Drag, mouseCoord) {

            var container = this.getContainer(mouseCoord);

            if (container) {
                if (container.attr("data-color")) {
                    if (img.attr("data-color") === container.attr("data-color")) {
                        this.fix(img, container);
                        Drag.setComplete();
                    } else {
                        Drag.returnToStart();
                        this.error(container);
                    }
                } else {
                    if (this.hasContainresWith(img.attr("data-color"))) {
                        Drag.returnToStart();
                        this.error(container);
                    } else {
                        this.fix(img, container);
                        Drag.setComplete();
                        this.saveFigure(img.attr("data-color"), container, "data-color");
                    }
                }
            } else {
                Drag.returnToStart();
            }

        }.bind(this));

    },

    isAllComplete: function () {

        return !(this.drag.some(function (Drag) {
            return !(Drag.isComplite());
        }));

    },

    getClassName: function (className) {
        var arrNames = className.split(" ");
        arrNames = arrNames.filter(function (elem) {
            if (elem != "error" && elem != "success") {
                return true;
            }
        });

        return arrNames.join(" ");
    },

    fix: function (img, container) {

        this.success(container);

        var offset = container.offset();
        var contentOffset = $(".content").offset();

        var className = this.getClassName(container.get(0).className);

        if (!this.complete) {
            this.complete = {};
        }

        if (!this.complete[className]) {
            this.complete[className] = [];
        }

        this.complete[className].push(img);

        img.animate({
            left: offset.left - contentOffset.left + ((this.complete[className].length - 1) * 100) + 10,
            top: offset.top - contentOffset.top + (container.height() - img.height()) / 2
        })

    },

    showFlicker: function (container, className) {
        var remove = function () {
                container.removeClass(className);
            },
            add = function () {
                container.addClass(className);
            };

        add();

        setTimeout(function () {
            remove();
        }, 400);
    },

    error: function (container) {
        this.showFlicker(container, "error");
    },

    success: function (container) {
        this.showFlicker(container, "success");
    },

    getContainer: function (coords) {

        var result = undefined;

        this.containers.each(function (index, container) {

            if (this.isOnContainer($(container), coords)) {
                result = $(container);
            }

        }.bind(this));

        return result;
    },

    isOnContainer: function (container, coords) {
        var offset = container.offset();
        return coords.x >= offset.left && coords.x <= offset.left + container.width() &&
            coords.y >= offset.top && coords.y <= offset.top + container.height();
    }

});

var start = new Start();