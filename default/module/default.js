/**
 * Created by daniil on 13.08.14.
 */
angular.module("default", [])
    .config(function ($stateProvider) {

        $stateProvider
            .state('home', {
                url: "/default",
                views: {
                    main: {
                        templateUrl: "./default/tpl/main.tpl.html"
                    }
                }
            })
    });